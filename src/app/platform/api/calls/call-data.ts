export const DATA = [
  {
    id: 1,
    title: 'Deck the Halls. Background Christmas',
    audio: '/assets/audio/deck-the-halls-background-christmas-music-for-short-video-vlog-172533.mp3',
    artist: 'White_Records'
  },
  {
    id: 2,
    title: 'Silent Night. Background Christmas',
    audio: '/assets/audio/silent-night-background-christmas-music-for-short-video-vlog-174363.mp3',
    artist: 'White_Records'
  },
  {
    id: 3,
    title: 'Stomps And Claps (Percussion and Rhythm)',
    audio: '/assets/audio/stomps-and-claps-percussion-and-rhythm-141190.mp3',
    artist: 'Alex_Kizenkov'
  },
  {
    id: 4,
    title: 'We Wish You a Merry Christmas',
    audio: '/assets/audio/we-wish-you-a-merry-christmas-short-background-christmas-vlog-music-172820.mp3',
    artist: 'Nane Simon'
  },
];
