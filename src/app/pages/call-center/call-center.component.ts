import {Component, ElementRef, OnInit, ViewChild} from "@angular/core";
import {MatTableModule} from "@angular/material/table";
import {DATA} from "../../platform/api/calls/call-data";
import {AudioPlayerComponent} from "ngx-audio-player";

@Component({
  imports: [
    MatTableModule
  ],
  selector: 'app-call-center',
  standalone: true,
  styleUrls: ['./call-center.component.scss'],
  templateUrl: './call-center.component.html',
})


export class CallCenterComponent implements OnInit {
  @ViewChild(AudioPlayerComponent) player: AudioPlayerComponent | undefined;
  @ViewChild('audioEl', {static: false}) audioEl: ElementRef | undefined;

  public playId: any = '';
  public audioUrl: any = '';
  pause: boolean = false;
  isPlaying: boolean = false;

  displayedColumns: string[] = ['id', 'title', 'artist', 'audio'];

  dataSource = DATA;


  ngOnInit(): void {
  }


  startPlay(item: any, audioEl: any) {
    this.playId = item.id;
    this.isPlaying = true;
    if (!this.audioUrl) {
      this.audioUrl = item.audio;
      setTimeout(() => {
        audioEl.play();
        this.pause = true;
      }, 100)
    } else if ((this.audioUrl == item.audio) && !this.pause) {
      this.pause = true;
      audioEl.play();
    } else if (this.audioUrl !== item.audio) {
      this.audioUrl = item.audio;
      setTimeout(() => {
        audioEl.play();
        this.pause = true;
      }, 100)
    } else {
      if (this.audioUrl == item.audio) {
        this.pause = false;
        audioEl.pause();
      }
    }
  }

  public closeAudio() {
    this.audioUrl = ''
    this.isPlaying = false;
    this.pause = true;
    this.playId = '';
  }
}
